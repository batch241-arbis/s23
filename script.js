let trainer = {};

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];
trainer.friends = {
	kanto: ["Brock", "Misty"],
	hoenn: ["May", "Max"]
};


trainer.talk = function() {
	console.log("Pikachu, I choose you!");
}

trainer.talk();

console.log(trainer);

console.log("Result of the dot notation:");
console.log(trainer.name);
console.log("Result of the square bracket notation:")
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();


function pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);
		target.health -= this.attack;

		console.log(target.name + "'s health is now reduced to " + target.health);

		if (target.health <= 0) {
			target.faint();
		}
	}

	this.faint = function() {
		console.log(this.name + " fainted.");
	}
}

let pikachu = new pokemon("Pikachu", 12);
let raichu = new pokemon("Raichu", 10);

console.log(pikachu);
pikachu.tackle(raichu);
pikachu.tackle(raichu);